<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
// $routes->setDefaultController('Home');
$routes->setDefaultController('SigninController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// $routes->get('/', 'Home::index');
$routes->get('/', 'SigninController::index');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

// Own custom routes
$routes->get('/', 'SignupController::index');
$routes->get('/signup', 'SignupController::index');
$routes->post('/signup', 'SignupController::store');
$routes->get('/signin', 'SigninController::index');
$routes->get('/dashboard', 'DashboardController::index',['filter' => 'authGuard']);

//Channel Management
$routes->get('/channel', 'ChannelController::index',['filter' => 'authGuard']);
$routes->get('/channel/new', 'ChannelController::create',['filter' => 'authGuard']);
$routes->post('/channel/new', 'ChannelController::store',['filter' => 'authGuard']);
$routes->get('/channel/list', 'ChannelController::list',['filter' => 'authGuard']);

//Programme Management
$routes->get('/programme', 'ProgrammeController::index',['filter' => 'authGuard']);
$routes->get('/programme/new', 'ProgrammeController::create',['filter' => 'authGuard']);
$routes->post('/programme/new', 'ProgrammeController::store',['filter' => 'authGuard']);
$routes->get('/programme/list', 'ProgrammeController::list',['filter' => 'authGuard']);

//Programme Shedule Management
$routes->get('/programmeshedule', 'ProgrammeSheduleController::index',['filter' => 'authGuard']);
$routes->get('/programmeshedule/new', 'ProgrammeSheduleController::create',['filter' => 'authGuard']);
$routes->post('/programmeshedule/store', 'ProgrammeSheduleController::store',['filter' => 'authGuard']);
$routes->get('/programmeshedule/list', 'ProgrammeSheduleController::list',['filter' => 'authGuard']);
$routes->get('/programmeshedule/programme', 'ProgrammeSheduleController::getProgramme',['filter' => 'authGuard']);