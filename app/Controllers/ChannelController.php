<?php 
namespace App\Controllers;  
use CodeIgniter\Controller;
use App\Models\ChannelModel;
use CodeIgniter\HTTP\IncomingRequest;

class ChannelController extends Controller
{
    var $session;
    public function __construct()
	{
		// Loading db instance
		$this->db = db_connect();
        $this->session = \Config\Services::session();
	}
    
    public function index()
    {
        echo view('layouts/header');
        echo view('channel/list');
        echo view('layouts/footer');
    } 

    public function create()
    {
        echo view('layouts/header');
        echo view('channel/create');
        echo view('layouts/footer');
    } 
    public function store()
    {
        $model = model(ChannelModel::class);
        if ($this->request->getMethod() === 'post' && $this->validate([
            'name' => 'required',
            'murl'  => 'required',
        ])) {
            
             $model->save([
                'name' => $this->request->getPost('name'),
                'country'  => $this->request->getPost('country'),
                'url'  => $this->request->getPost('murl'),
                'status'  => 1,
            ]);
            
            $this->session->setFlashdata('success', 'Channel Created successfully');
            return redirect()->to('/channel/new');
           
        } 
    } 
    public function list()
    {
       
        $builder = $this->db->table('channel');
        $builder->select('channel.name As channel_name,country.name As country_name,channel.url As url');
        $builder->join('country', 'channel.country = country.id');
        $query = $builder->where('channel.status',1)->get();

        $model = model(ChannelModel::class);
        // Datatables Variables
        $draw = intval($this->request->getGet("draw"));
        $start = intval($this->request->getGet("start"));
        $length = intval($this->request->getGet("length"));
  
        $data = array();
        $i=1;
        
        foreach($query->getResult() as $r) {
          
                  $data[] = array(
                    $i++,
                    $r->channel_name,
                    $r->country_name,
                    $r->url,
                    '<input type="button" value="delete"></>'

                );
        }

        $output = array(
                "draw" => $draw,
                "recordsTotal" => 10,
                "recordsFiltered" => 10,
                "data" => $data
            );
        echo json_encode($output);
    } 



  
}