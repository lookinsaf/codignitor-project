<?php 
namespace App\Controllers;  
use CodeIgniter\Controller;
  
class DashboardController extends Controller
{
    public function index()
    {
        echo view('layouts/header');
        echo view('dashboard');
        echo view('layouts/footer');
    } 
  
}