<?php 
namespace App\Controllers;  
use CodeIgniter\Controller;
use App\Models\ChannelModel;
use App\Models\ProgrammeModel;
use CodeIgniter\HTTP\IncomingRequest;

class ProgrammeController extends Controller
{
    var $session;
    public function __construct()
	{
		// Loading db instance
		$this->db = db_connect();
        $this->session = \Config\Services::session();
	
        
	}
    
    public function index()
    {
        echo view('layouts/header');
        echo view('programme/list');
        echo view('layouts/footer');
    } 

    public function create()
    {
        $builder = $this->db->table('channel');
        $query = $builder->get();
        $channels=$query->getResult();
        $chanel_array = array();
        foreach($query->getResult() as $r) {
            $chanel_array[] = array(
              $r->name,
              $r->id
          );
        }
        $data['channel_data']=$chanel_array;
        echo view('layouts/header');
        echo view('programme/create',$data);
        echo view('layouts/footer');
    } 
    public function store()
    {
        
        $model = model(ProgrammeModel::class);
        if ($this->request->getMethod() === 'post' && $this->validate([
            'name' => 'required',
            'channel_id'  => 'required',
        ])) {
            
             $model->save([
                'name' => $this->request->getPost('name'),
                'channel_id'  => $this->request->getPost('channel_id'),
                'status'  => 1,
            ]);
            $this->session->setFlashdata('success', 'Programme Created successfully');
            return redirect()->to('/programme/new');
           
        } 
    } 
    public function list()
    {
        $builder = $this->db->table('programme');
        $builder->select('programme.name As programme_name,channel.name As channel_name');
        $builder->join('channel', 'programme.channel_id = channel.id');
        $query = $builder->get();

        // Datatables Variables
        $draw = intval($this->request->getGet("draw"));
        $start = intval($this->request->getGet("start"));
        $length = intval($this->request->getGet("length"));
  
        $data = array();
        $i=1;
        
        foreach($query->getResult() as $r) {
          
                  $data[] = array(
                    $i++,
                    $r->programme_name,
                    $r->channel_name
                );
        }

        $output = array(
                "draw" => $draw,
                "recordsTotal" => 10,
                "recordsFiltered" => 10,
                "data" => $data
            );
        echo json_encode($output);
    } 



  
}