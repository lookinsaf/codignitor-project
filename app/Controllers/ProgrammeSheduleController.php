<?php 
namespace App\Controllers;  
use CodeIgniter\Controller;
use App\Models\ChannelModel;
use App\Models\ProgrammeModel;
use App\Models\ProgrammeSheduleModel;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\Files\File;

class ProgrammeSheduleController extends Controller
{
    var $session;
    public function __construct()
	{
		// Loading db instance
		$this->db = db_connect();
        $this->session = \Config\Services::session();
        
	}
    
    public function index()
    {
        
    } 

    public function create()
    {
        
        $builder = $this->db->table('channel');
        $query = $builder->where('status',1)->get();
        $channel_array = array();
        foreach($query->getResult() as $r) {
            $channel_array[] = array(
              $r->name,
              $r->id
          );
        }
        $data['channel_data']=$channel_array;
        echo view('layouts/header');
        echo view('programmeshedule/shedule',$data);
        echo view('layouts/footer');
    } 
    public function store()
    { 
        
        if(isset($_REQUEST['start'])){
            $length = count($_REQUEST['start']);
            $files = $this->request->getFiles();
            $builder = $this->db->table('programme_shedule');
            
            for ($i = 0; $i < $length; $i++) {
                $filepath='';
                if(file_exists( $files['image'][$i])){
                    $filepath = WRITEPATH . 'uploads/' . $files['image'][$i]->store();
                }
                
                $st = strtotime($_REQUEST['start'][$i]);
                $st1 = date('H:i:s', $st);
    
                $single_array=['programme_id'=>$_REQUEST['programme_id'][$i],
                "start_at"=>$st1,"end_at"=>$_REQUEST['end'][$i],
                "image"=>$filepath,"description"=>$_REQUEST['description'][$i]];
    
                $builder->insert($single_array);
            }
            $this->session->setFlashdata('success', 'Programme Shedule Created successfully');
            return redirect()->to('/programmeshedule/new');
            
        }else{
            $this->session->setFlashdata('success', 'Fill The Data');
            return redirect()->to('/programmeshedule/new');
        }
       
        
    } 
    

    public function getProgramme()
    {
        $id=$this->request->getGet("id");
        $builder = $this->db->table('programme');
        $query = $builder->where('channel_id', $id)->get();
        $programme_array = array();
        foreach($query->getResult() as $r) {
            $programme_array[] = array(
              $r->name,
              $r->id
          );
        }
        echo json_encode($programme_array);
      
    }



  
}