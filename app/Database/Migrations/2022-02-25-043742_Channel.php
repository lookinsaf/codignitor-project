<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Channel extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'auto_increment' => true
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '100'
            ],
            'country' => [
                'type' => 'INT'
            ],
            'url' => [
                'type' => 'TEXT'
            ],
            'status' => [
                'type' => 'INT',
                'default' => 1,
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('channel');
    }

    public function down()
    {
        $this->forge->dropTable('channel');
    }
}
