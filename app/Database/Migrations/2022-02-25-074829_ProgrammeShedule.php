<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ProgrammeShedule extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'auto_increment' => true
            ],
            'programme_id' => [
                'type' => 'INT'
            ],
            'start_at' => [
                'type' => 'TIME'
            ],
            'end_at' => [
                'type' => 'TIME'
            ],
            'image' => [
                'type' => 'TEXT'
            ],
            'description' => [
                'type' => 'TEXT'
            ],
            'created_at datetime default current_timestamp',
            'updated_at datetime default current_timestamp on update current_timestamp'
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('programme_shedule');
    }

    public function down()
    {
        $this->forge->dropTable('programme_shedule');
    }
}
