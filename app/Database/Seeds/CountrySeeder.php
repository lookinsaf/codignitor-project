<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CountrySeeder extends Seeder
{
    public function run()
    {
        $data1 = [
            'name' => 'SRI LANKA',
            'status'    => 1
        ];
        $data2 = [
            'name' => 'INDIA',
            'status'    => 1
        ];
        $data3 = [
            'name' => 'AMERICA',
            'status'    => 1
        ];

        // Using Query Builder
        $this->db->table('country')->insert($data1);
        $this->db->table('country')->insert($data2);
        $this->db->table('country')->insert($data3);
    }
}
