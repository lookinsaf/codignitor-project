<?php 
namespace App\Models;  
use CodeIgniter\Model;
  
class ChannelModel extends Model{
    protected $table = 'channel';
    
    protected $allowedFields = [
        'name',
        'country',
        'url',
        'status',
        'created_at'
    ];

    public function get_channel()
     {
        $builder = $db->table('channel');
        return $query   = $builder->get();
          return $this->db->get("channel");
     }
}