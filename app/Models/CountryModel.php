<?php 
namespace App\Models;  
use CodeIgniter\Model;
  
class CountyryModel extends Model{
    protected $table = 'country';
    
    protected $allowedFields = [
        'name',
        'status',
        'created_at'
    ];
}