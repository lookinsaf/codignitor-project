<?php 
namespace App\Models;  
use CodeIgniter\Model;
  
class ProgrammeModel extends Model{
    protected $table = 'programme';
    
    protected $allowedFields = [
        'name',
        'channel_id',
        'status'
    ];

   
}