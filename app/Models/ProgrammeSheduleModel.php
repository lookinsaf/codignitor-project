<?php 
namespace App\Models;  
use CodeIgniter\Model;
  
class ProgrammeSheduleModel extends Model{
    protected $table = 'programme_shedule';
    
    protected $allowedFields = [
        'programme_id ',
        'start_at',
        'end_at',
        'image',
        'description',
    ];

   
}