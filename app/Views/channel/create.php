
<div class="clearfix">
<?= session()->getFlashdata('error') ?>

<?= service('validation')->listErrors() ?>

</div>
<div class="content-wrapper">
<div class="container-fluid">
   <div class="row">
      <div class="col-md-10">
         <h5 class="w-75 p-3 text-uppercase">New Channel</h5>
      </div>
      <div class="col-md-2 float-right justify-content-md-center">
         <button type="button" class="btn btn-light btn-block" onclick="window.location.href='<?php echo base_url('/channel'); ?>'" ><i class="fa fa-list">View All</i> </button>   
      </div>
   </div>
   <form method="POST" action="<?php echo base_url('/channel/new'); ?>" >
    <?= csrf_field() ?>
      <div class="row mt-3">
         <div class="col-lg-10">
            <div class="card">
               <div class="card-body">
                  <div class="card-title">Channel Details</div>
                  <hr>
                  <div class="form-group">
                     <label for="input-1">Name</label>
                     <input type="text" name="name" class="form-control" id="input-1" placeholder="Enter Channel Name" required>
                  </div>
                  <div class="form-group">
                     <label for="input-3">Country</label>
                     <select class="form-control" name="country" id="exampleFormControlSelect1" required>
                        <option value="1">Sri Lanka</option>
                        <option value="2">India</option>
                        <option value="3">America</option>
                     </select>
                  </div>
                  
                  <div class="form-group">
                     <label for="input-3"> M3U8 Url</label>
                     <input type="url" name="murl" class="form-control" id="input-1" placeholder="Enter M3U8 Url" required>
                  </div>
                 
                  <div class="form-group">
                     <button type="submit" class="btn btn-light px-5 primary"><i class="fa fa-save"></i> Save</button>
                     <?= session()->getFlashdata('success') ?>
                  </div>
                  
               </div>
            </div>
         </div>
        
      </div>
   </form>
</div>


