
<div class="clearfix"></div>
<div class="content-wrapper">
<div class="container-fluid">
   <div class="row">
      <div class="col-md-10">
         <h3 class="w-75 p-3 text-uppercase">Channel List</h3>
         <?= session()->getFlashdata('success') ?>
      </div>
      <div class="col-md-2 float-right justify-content-md-center">
         <button type="button" class="btn btn-light btn-block" onclick="window.location.href='<?php echo base_url('/channel/new'); ?>'" ><i class="fa fa-plus">New</i> </button>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12">
         <div class="card">
            <div class="card-body">
               <div class="table-responsive">
                <table id="example" class="table  table-striped table-bordered " width="100%">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Country</td>
                            <td>Url</td>
                            <td>Action</td>
                            <!-- <td width="1%">Active/ Deactivate</td>
                            <td width="1%">Edit</td> -->
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--End Row-->
</div>

<!-- Script Here -->

<script type="text/javascript">

var table = $('#example').DataTable({
        responsive: {
        details: true
    },
        ajax: '<?php echo base_url("channel/list"); ?>',
        type : 'GET',
        buttons: [
            'copy', 'excel', 'pdf'
         ]
        
    });
</script>
