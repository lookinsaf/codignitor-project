
<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="card mt-3">
        <div class="card-content">
            <div class="row row-group m-0">
                <div class="col-12 col-lg-6 col-xl-3 border-light">
                    <div class="card-body">
                      <h5 class="text-white mb-0">5<span class="float-right"><i class="fa fa-shopping-cart"></i></span></h5>
                        <div class="progress my-3" style="height:3px;">
                          <div class="progress-bar" style="width:55%"></div>
                        </div>
                      <p class="mb-0 text-white small-font">Total Channels <span class="float-right">Demo<i class="zmdi zmdi-long-arrow-up"></i></span></p>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-3 border-light">
                    <div class="card-body">
                      <h5 class="text-white mb-0">8 <span class="float-right"><i class="fa fa-money"></i></span></h5>
                        <div class="progress my-3" style="height:3px;">
                          <div class="progress-bar" style="width:55%"></div>
                        </div>
                      <p class="mb-0 text-white small-font">Total Programme <span class="float-right">Demo <i class="zmdi zmdi-long-arrow-up"></i></span></p>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-3 border-light">
                    <div class="card-body">
                      <h5 class="text-white mb-0">10 <span class="float-right"><i class="fa fa-arrow-up"></i></span></h5>
                        <div class="progress my-3" style="height:3px;">
                          <div class="progress-bar" style="width:55%"></div>
                        </div>
                      <p class="mb-0 text-white small-font">Sheduled Count <span class="float-right"> Demo<i class="zmdi zmdi-long-arrow-up"></i></span></p>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-3 border-light">
                
            </div>
            
            
        </div>
      </div>

    
    
    </div>  
 
