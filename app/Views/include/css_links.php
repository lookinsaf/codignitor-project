<!-- loader-->
  <link href="<?php echo base_url(); ?>/assets/template_1/css/pace.min.css" rel="stylesheet"/>
  <script src="<?php echo base_url(); ?>/assets/template_1/js/pace.min.js"></script>
  <!--favicon-->
  <link rel="icon" href="<?php echo base_url(); ?>/assets/template_1/images/favicon.ico" type="image/x-icon">
  <!-- simplebar CSS-->
  <link href="<?php echo base_url(); ?>/assets/template_1/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url(); ?>/assets/template_1/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="<?php echo base_url(); ?>/assets/template_1/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="<?php echo base_url(); ?>/assets/template_1/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="<?php echo base_url(); ?>/assets/template_1/css/sidebar-menu.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="<?php echo base_url(); ?>/assets/template_1/css/app-style.css" rel="stylesheet"/>
  <!-- Toaster CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/template_1/css/toastr.min.css">
  <!-- Datatble -->
  <link href="<?php echo base_url(); ?>/assets/template_1/css/dataTables.bootstrap4.min.css" rel="stylesheet">
