<!DOCTYPE html>
<html lang="en">
   <head>
   <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta charset="utf-8"/>
      <meta http-equiv="content-type" content="text/html;charset=utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
      <meta name="description" content=""/>
      <meta name="author" content=""/>
      <title>TV Management</title>
      <!-- loader-->
      <?=$this->include("include/css_links")?>
     
   </head>
   <body class="bg-theme bg-theme1">
      <!-- start loader -->
      <div id="pageloader-overlay" class="visible incoming">
         <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner" >
               <div class="loader"></div>
            </div>
         </div>
      </div>
      <!-- end loader -->
      <!-- Start wrapper-->
      <div id="wrapper">
         <!--Start sidebar-wrapper-->
         <?=$this->include("layouts/side-bar")?>
         <!--End sidebar-wrapper-->
         <!--Start topbar header-->
         <?=$this->include("layouts/top-bar")?>
         <!--End topbar header-->
         <?=$this->include("include/js_links")?>