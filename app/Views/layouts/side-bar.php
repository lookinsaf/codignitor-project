<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
     <div class="brand-logo">
      <a href="index.html">
       <img src="<?php echo base_url(); ?>/assets/template_1/images/logo-icon.png" class="logo-icon" alt="logo icon">
       <h5 class="logo-text">TV MANAGEMENT</h5>
     </a>
   </div>
   <ul class="sidebar-menu do-nicescrol">
      <li class="sidebar-header">MAIN NAVIGATION</li>
      <li>
        <a href="<?php echo base_url('dashboard'); ?>">
          <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url('channel'); ?>">
          <i class="zmdi zmdi-account-box"></i> <span>Channel</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url('programme'); ?>">
          <i class="zmdi zmdi-format-list-bulleted"></i> <span>Programme</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url('programmeshedule/new'); ?>">
          <i class="zmdi zmdi-grid"></i> <span>Programe Shedule</span>
        </a>
      </li>

    </ul>
   
   </div>