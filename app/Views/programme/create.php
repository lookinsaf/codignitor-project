
<div class="clearfix">
<?= session()->getFlashdata('error') ?>
<?= service('validation')->listErrors() ?>

</div>
<div class="content-wrapper">
<div class="container-fluid">
   <div class="row">
      <div class="col-md-10">
         <h5 class="w-75 p-3 text-uppercase">New Programme</h5>
      </div>
      <div class="col-md-2 float-right justify-content-md-center">
         <button type="button" class="btn btn-light btn-block" onclick="window.location.href='<?php echo base_url('/programme'); ?>'" ><i class="fa fa-list">View All</i> </button>   
      </div>
   </div>
   <form method="POST" action="<?php echo base_url('/programme/new'); ?>" >
    <?= csrf_field() ?>
      <div class="row mt-3">
         <div class="col-lg-10">
            <div class="card">
               <div class="card-body">
                  <div class="card-title">Programme Details</div>
                  <hr>
                  <div class="form-group">
                     <label for="input-1">Name</label>
                     <input type="text" name="name" class="form-control" id="input-1" placeholder="Enter Programme Name" required>
                  </div>
                  <div class="form-group">
                     <label for="input-3">Channel</label>
                     <select class="form-control" name="channel_id" id="exampleFormControlSelect1" required>
                        <?php foreach ($channel_data as $channel) {?>
                        <option value="<?php echo $channel[1] ?><"><?php echo $channel[0] ?></option>
                        <?php }?>

                     </select>
                  </div>
                 
                  <div class="form-group">
                     <button type="submit" class="btn btn-light px-5 primary"><i class="fa fa-save"></i> Save</button>
                     <?= session()->getFlashdata('success') ?>
                  </div>
                  
               </div>
            </div>
         </div>
        
      </div>
   </form>
</div>


