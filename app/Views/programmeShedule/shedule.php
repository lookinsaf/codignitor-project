
<div class="clearfix"></div>
<div class="content-wrapper">
<div class="container-fluid">
<form method="POST" action="<?php echo base_url('/programmeshedule/store'); ?>" enctype="multipart/form-data">
<?= csrf_field() ?>
   <div class="row">
      <div class="col-md-8">
         <h3 class="w-75 p-3 text-uppercase">Programme Sheduller</h3>
      </div>
      
      <div class="col-md-2 float-right justify-content-md-center">
         <select class="form-control" id="channel_id" name="channel_id" id="exampleFormControlSelect1" required >
            <?php foreach ($channel_data as $channel) {?>
            <option value="<?php echo $channel[1] ?>"><?php echo $channel[0] ?></option>
            <?php }?>

         </select>
      </div>
      <div class="col-md-2 float-right justify-content-md-center">
         <input type="date" class="btn btn-light btn-block" name="shedule_date" required>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-8">
      
         <div class="card">
           
            <div class="card-body">
               <div class="table-responsive">
                <table id="example_shedule" class="table  table-striped table-bordered " width="100%">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Programme Name</td>
                            <td>Start Time</td>
                            <td>End Time</td>
                            <td>Image</td>
                            <td>Description</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
               </div>
            </div>
            
         </div>
         <div class="form-group right">
            <button type="submit" class="btn btn-light px-5 primary"><i class="fa fa-save"></i> Save</button>
            <?= session()->getFlashdata('success') ?>
        </div>
        
      </div>
      <div class="col-lg-4">
         <div class="card">
            <div class="card-body">
               <div class="table-responsive">
                <table id="example_programme" class="table  table-striped table-bordered " width="100%">
                        <thead>
                        <tr>
                            <td>Programme</td>
                            <td>Label</td>
                        </tr>
                        </thead>
                        <tbody id="programme_body">
                        
                        </tbody>
                    </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   </form>
   <!--End Row-->
</div>

<!-- Script Here -->

<script type="text/javascript">

var table = $('#example_shedule').DataTable();
var count=1;
loadProgramme();

function add(name,id){

   table.row.add( [
      count ++,
      name,
      '<input type="hidden" id="appt" value="'+id+'" name="programme_id[]"><input type="time" id="appt" name="start[]" required>',
      '<input type="time" id="appt" name="end[]" required>',
      '<input type="file" name="image[]"></input>',
      '<textarea name="description[]"></textarea>',
      '<input type="button" onclick="delete_row()" class="remove" value="Delete">'
   ]).draw();

}
function delete_row(){
   $('#example_shedule').on('click', '.remove', function() {
      table
      .row($(this).parents('tr'))
      .remove()
      .draw();
   
  });
}

function loadProgramme(){
   $('#programme_body tr').remove();
   $.ajax({
      url: "<?php echo base_url("programmeshedule/programme"); ?>", 
      data: {id: $('#channel_id').val()},
      success: function(result){
         const data = JSON.parse(result);
         $.each( data, function( key, value ) {
            $('#programme_body').append(
               '<tr>'+
               '<td>'+
               value[0] +
               '</td>'+
               '<td><input type="button" value="ADD" onclick="add('+"'"+value[0]+"'"+','+value[1]+')"></>'+
               '</td>'+
               '</tr>'
            );
         
           
         });
         
      }
  });
}

$('#channel_id').on('change', function() {
   loadProgramme();
});



</script>
